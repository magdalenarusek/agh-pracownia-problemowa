# AGH Pracownia Problemowa

Repozytorium zawiera pliki:

1.  /Scale/scale.ipynb - jego zadaniem jest wykrycie poziomej czerwonej podziałki na zdjęciu, obliczenie jego długości oraz zapisanie kopii zdjęcia z obliczoną długością w nazwie nowego pliku


1. /Feature engineering - zawiera pliki, za pomocą których były testowane różne kombinacje klasyfikacji struktury ze zdjęcia:
- moments_old_data.ipynb
- moments_old_data_same_size.ipynb
- moments_new_data_more_methods.ipynb
- moments_new_data_same_size_more_methods.ipynb

Testowane klasyfikatory:
 - SVM, 
 - Decision Tree,
 - Random Forest, 
 - Logistic Regression,
 - Multi-layer Perceptron,
 - AdaBoost.

 Na wejście modelu podawano różne informacje: 
 - momenty Hu, 
 - momenty Zernike, 
 - zestaw momentów Zernike oraz Hu,
 - piksele.

Testy przeprowadzono z wykorzystaniem przygotowanego uprzednio zbioru obrazów podzielonych na klasy: I, II, III, IV, V oraz VI oraz nowego, zmodyfikowanego zbioru uwzględniającego zalecenia specjalisty danej dziedziny. Dodatkowo testy wykonywano dwukrotnie dla nowego i starego zbioru, podzieliwszy je na zbiór pełny oraz zbiór pomniejszony (usunięto klasę II oraz IV, a pozostałe zmniejszono do jednakowego rozmiaru - 278 przykładów każdy).

Pliki były uruchamiane z wykorzystaniem narzędzia Jupyter Notebook z Pythonem w wersji 3.8.5.
